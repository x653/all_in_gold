# All In Gold

The master of archery sight calibration.

All In Gold helps you find the best sight calibration for your bow.
Shot arrows from three or more distances. Enter the position of the
hits on the target and the app will calculate the best sight adjustment
for any other distance. All In Gold uses geometric and statistical algorithm.
Keep on entering more arrows to improve the accuracy of the calibration.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/x653.all_in_gold/)