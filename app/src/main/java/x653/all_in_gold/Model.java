/*
all_in_gold, the master of archery sight calibration.
Copyright (C) 2019 Michael Schröder (mi.schroeder@netcologne.de)

This file is part of all_in_gold.

all_in_gold is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

all_in_gold is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with all_in_gold.  If not, see <http://www.gnu.org/licenses/>.
*/

package x653.all_in_gold;
import java.util.ArrayList;
import java.util.Observer;

// Model verwaltet GoGorGold

class Model {
    private final Targets targets;
    private final Arrows arrows;
    private final SightSetting elevation;
    private final SightSetting windage;
    private final Distance distance;
    private final AutoCalibrator autoCalibrator;

    public Model(){
        targets = new Targets();
        arrows = new Arrows();
        elevation = new SightSetting();
        windage = new SightSetting();
        distance = new Distance();
        autoCalibrator = new AutoCalibrator(distance,elevation);
    }

    void load(String string) {
        arrows.loadArrows(CSV.parseArrows(string));
    }
    String save() {
        return CSV.toCSVString(arrows.getArrows());
    }
    boolean isFIELD(){ return targets.isFIELD();}
    boolean isFITA(){return targets.isFITA();}
    void setFIELD(){targets.setFIELD();}
    void setFITA(){targets.setFITA();}
    Target[] getTargets(){return targets.getTargets();}
    Target getTarget(){return targets.getTarget();}
    void setTarget(int v){targets.setTarget(v);}
    public void addTargetObserver(Observer observer){
        targets.addObserver(observer);
    }

    void action(){arrows.action();}

    void addArrow(Punkt punkt){arrows.addArrow(new Arrow(distance.getCM(),elevation.getCM(),punkt.getX(),punkt.getY()));}
    float getAverage(){return Scorer.getAverage(targets.getTarget(),getArrows());}
    float getAveragePasse(){return Scorer.getAverage(targets.getTarget(),getLast());}
    String getPasseScore(){return Scorer.getPoints(targets.getTarget(),getLast());}
    void addArrowsObserver(Observer o){arrows.addObserver(o);}
    void deleteAll(){arrows.deleteAll();}
    void deleteLast(){arrows.deleteLast();}
    ArrayList<Arrow> getPasse(){return arrows.getPasse();}
    ArrayList<Arrow> getArrows(){return Corrector.changeAll(arrows.getArrows(),distance.getCM(),elevation.getCM(),windage.getCM());}
    ArrayList<Arrow> getLast(){return Corrector.changeAll(arrows.getPasse(),distance.getCM(),elevation.getCM(),windage.getCM());}

    void elevationInc(){elevation.inc();}
    void elevationDec(){elevation.dec();}
    void setElevation(float v){elevation.setValue(v);}
    float getElevationValue(){return elevation.getValue();}
    String getElevation(){return elevation.getString();}
    void addElevationObserver(Observer o){elevation.addObserver(o);}


    Integer getDistanceLength(){return distance.getLength();}
    Length_Unit getDistanceUnit(){return distance.getUnit();}
    void setDistanceUnit(Length_Unit v){distance.changeUnit(v);}
    void setDistance(int v){distance.setLength(v);}

    void distanceInc(){distance.inc();}
    void distanceDec(){distance.dec();}
    void addDistanceObserver(Observer o){distance.addObserver(o);}


    void windageInc(){windage.inc();}
    void windageDec(){windage.dec();}
    void setWindage(float v){windage.setValue(v);}
    float getWindageValue(){return windage.getValue();}
    String getWindage(){return windage.getString();}
    SightSetting.UNIT getWindageUnit(){return windage.getUnit();}
    void addWindageObserver(Observer o){windage.addObserver(o);}

    void setAutoCalibrator(boolean enabled){
        if (enabled) autoCalibrator.setSwitchON();
        else autoCalibrator.setSwitchOFF();
    }
    boolean getAutoCalibrator(){return autoCalibrator.getSwitch();}
    void addAutoCalibratorObserver(Observer o){autoCalibrator.addObserver(o);}
    void setElevationUnit(SightSetting.UNIT v){elevation.setUnit(v);}
    SightSetting.UNIT getElevationUnit() {return elevation.getUnit();}
    float getEye(){return Corrector.getEye();}
    void setEye(float v){Corrector.setEye(v);}
    boolean undoo(){
        return arrows.removeLastArrow();
    }

}