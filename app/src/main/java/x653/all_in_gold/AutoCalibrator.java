/*
all_in_gold, the master of archery sight calibration.
Copyright (C) 2019 Michael Schröder (mi.schroeder@netcologne.de)

This file is part of all_in_gold.

all_in_gold is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

all_in_gold is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with all_in_gold.  If not, see <http://www.gnu.org/licenses/>.
*/

package x653.all_in_gold;
import java.util.Observable;
import java.util.Observer;

//  AutoCalibrator koppelt distance und elevation

class AutoCalibrator extends Observable {
    private final Observer checkDistance;
    private boolean theSwitch;

    public AutoCalibrator(final Distance distance, final SightSetting elevation){
        theSwitch = false;
        checkDistance = new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                float bestSight = Corrector.bestSight(distance.getCM());
                if (theSwitch) {
                    elevation.setCM(bestSight);
                }
                Corrector.QUALITY quality=Corrector.getQuality(distance.getCM(),elevation.getCM());
                setChanged();
                notifyObservers(quality);
            }
        };
        Observer checkElevation = new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                Corrector.QUALITY quality = Corrector.getQuality(distance.getCM(), elevation.getCM());
                if (theSwitch) {
                    if (quality != Corrector.QUALITY.IS_FIX)
                        setSwitchOFF();
                }
                setChanged();
                notifyObservers(quality);
            }
        };
        elevation.addObserver(checkElevation);
        distance.addObserver(checkDistance);
    }
    public void setSwitchON(){
        if (!theSwitch) {
            theSwitch=true;
            checkDistance.update(null,null);
            setChanged();
            notifyObservers(theSwitch);
        }
    }
    public void setSwitchOFF(){
        if (theSwitch){
            theSwitch=false;
            setChanged();
            notifyObservers(theSwitch);
        }
    }
    boolean getSwitch(){return theSwitch;}

}
