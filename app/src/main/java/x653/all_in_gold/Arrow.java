/*
all_in_gold, the master of archery sight calibration.
Copyright (C) 2019 Michael Schröder (mi.schroeder@netcologne.de)

This file is part of all_in_gold.

all_in_gold is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

all_in_gold is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with all_in_gold.  If not, see <http://www.gnu.org/licenses/>.
*/

package x653.all_in_gold;

// Arrow verwaltet einen Pfeil

class Arrow extends Punkt{
    private final float distance;                   //in cm
    private final float elevation;                  //in cm
    private static final float radius=(float)0.5;

    Arrow(float distance, float elevation, float x, float y){
        super(x,y);
        this.distance = distance;
        this.elevation = elevation;
    }

    float getDistance(){return distance;}
    float getElevation(){return elevation;}
    float getR(){return Math.max(0,super.getR()- radius);}
    static float getRadius(){return radius;}

}