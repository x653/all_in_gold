/*
all_in_gold, the master of archery sight calibration.
Copyright (C) 2019 Michael Schröder (mi.schroeder@netcologne.de)

This file is part of all_in_gold.

all_in_gold is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

all_in_gold is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with all_in_gold.  If not, see <http://www.gnu.org/licenses/>.
*/

package x653.all_in_gold;
import java.util.Locale;
import java.util.Observable;

// SightSetting verwaltet eine Visiereinstellung

class SightSetting extends Observable {

    enum STEP {
        STEP_1MM,STEP_05MM,STEP_02MM,STEP_01MM,STEP_32IN;
        float getCM(){
            switch (this){
                case STEP_1MM:
                    return (float)0.1;
                case STEP_05MM:
                    return (float)0.05;
                case STEP_02MM:
                    return (float)0.02;
                case STEP_01MM:
                    return (float)0.01;
                case STEP_32IN:
                    return (float)2.54/32;
            }
            return 0;
        }
    }

    enum UNIT {
        UNIT_20MM,UNIT_10MM,UNIT_1MM,UNIT_1IN;
        float getCM(){
            switch (this){
                case UNIT_20MM:
                    return 2;
                case UNIT_10MM:
                    return 1;
                case UNIT_1MM:
                    return (float)0.1;
                case UNIT_1IN:
                    return (float)2.54;
            }
            return 0;
        }
        public String toString(){
            switch (this){
                case UNIT_20MM:
                    return "[2cm]";
                case UNIT_10MM: return "[cm]";
                case UNIT_1MM: return "[mm]";
                case UNIT_1IN: return "[inch]";
            }
            return null;
        }
    }

    private STEP step;
    private UNIT unit;
    private float position;           // in 1/10 mm

    SightSetting(){
        unit = UNIT.UNIT_1MM;
        step = STEP.STEP_1MM;
        position = Integer.MIN_VALUE;
    }

    void inc(){
        setCM(position+step.getCM());
    }

    void dec(){
        setCM(position-step.getCM());
    }

    float getCM(){
        return position;
    }
    void setCM(float p){
        position=Math.round(p/step.getCM())*step.getCM();
        setChanged();
        notifyObservers(getString());
    }

    float getValue(){return position/unit.getCM();}
    void setValue(float p){
        setCM(Math.round(p*unit.getCM()/step.getCM())*step.getCM());
    }

    UNIT getUnit(){return unit;}
    void setUnit(UNIT pUnit){
        if (unit!=pUnit){
            float p = getValue();
            unit = pUnit;
            switch (unit){
                case UNIT_1IN:step=STEP.STEP_32IN;break;
                case UNIT_10MM:case UNIT_20MM:step=STEP.STEP_1MM;break;
            }
            setValue(p);
        }
    }

    STEP getStep(){
        return step;
    }
    void setStep(STEP pStep){
        if (step!=pStep) {
            float p = getCM();
            step = pStep;
            setCM(p);
        }
    }

    String getString(){
        switch (unit){
            case UNIT_10MM:
                if (step==STEP.STEP_1MM)
                        return String.format(Locale.getDefault(),"%.1f", position /unit.getCM());
                return String.format(Locale.getDefault(),"%.2f", position /unit.getCM());
            case UNIT_20MM:
                if (step==STEP.STEP_1MM || step==STEP.STEP_02MM)
                    return String.format(Locale.getDefault(),"%.2f", position /unit.getCM());
                return String.format(Locale.getDefault(),"%.3f", position /unit.getCM());
            case UNIT_1MM:
                if (step==STEP.STEP_1MM)
                    return String.format(Locale.getDefault(),"%.0f", position /unit.getCM());
                return String.format(Locale.getDefault(),"%.1f", position /unit.getCM());
            case UNIT_1IN:
                int inch=(int)(position/unit.getCM());
                int rest=(int)((position/unit.getCM()-inch)*32);
                int q=32;
                while (rest>1 && rest%2==0){
                    rest=rest/2;
                    q=q/2;
                }
                return String.format(Locale.getDefault(),"%d %d/%d",inch,rest,q);
        }
        return null;
    }

}
