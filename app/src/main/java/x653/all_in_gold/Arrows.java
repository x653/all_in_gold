/*
all_in_gold, the master of archery sight calibration.
Copyright (C) 2019 Michael Schröder (mi.schroeder@netcologne.de)

This file is part of all_in_gold.

all_in_gold is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

all_in_gold is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with all_in_gold.  If not, see <http://www.gnu.org/licenses/>.
*/

package x653.all_in_gold;

import java.util.ArrayList;
import java.util.Observable;

//  Arrows verwaltet alle Pfeile

class Arrows extends Observable {
    private final ArrayList<Arrow> arrows;
    private final ArrayList<Arrow> passe;
    private boolean isAdding;

    Arrows(){
        arrows = new ArrayList<>();
        passe = new ArrayList<>();
        deleteAll();
    }

    void action(){
        if (isAdding){
            addArrows();
        }else {
            newPasse();
        }
    }

    void deleteAll(){
        arrows.clear();
        passe.clear();
        Corrector.newRegression();
        isAdding=false;
        setChanged();
        notifyObservers(false);
    }

    void deleteLast(){
        if (!isAdding) {
            for (Arrow arrow : passe) {
                arrows.remove(arrow);
            }
            passe.clear();
            Corrector.newRegression();
            Corrector.addArrows(arrows);
            setChanged();
            notifyObservers(false);
        }
    }

    void loadArrows(ArrayList<Arrow> arrows){
        if (arrows != null) {
            this.arrows.clear();
            this.arrows.addAll(arrows);
            Corrector.newRegression();
            Corrector.addArrows(arrows);
            isAdding = false;
            setChanged();
            notifyObservers(false);
        }
    }

    ArrayList<Arrow> getArrows()
    {
        return arrows;
    }
    ArrayList<Arrow> getPasse(){
        return passe;
    }
    private void addArrows(){
        this.arrows.addAll(passe);
        Corrector.addArrows(passe);
        isAdding=false;
        setChanged();
        notifyObservers(false);
    }

    void addArrow(Arrow arrow){
        if (isAdding) {
            passe.add(arrow);
            setChanged();
            notifyObservers(arrow);
        }
    }

    boolean removeLastArrow(){
        if (!isAdding) return false;
        if (passe.size()>0){
            passe.remove(passe.size()-1);
            setChanged();
            notifyObservers(null);
            return true;
        }
        setChanged();
        notifyObservers(false);
        isAdding=false;
        return true;
    }

    private void newPasse(){
        if (!isAdding) {
            passe.clear();
            isAdding = true;
            setChanged();
            notifyObservers(true);
        }
    }

}
