/*
all_in_gold, the master of archery sight calibration.
Copyright (C) 2019 Michael Schröder (mi.schroeder@netcologne.de)

This file is part of all_in_gold.

all_in_gold is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

all_in_gold is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with all_in_gold.  If not, see <http://www.gnu.org/licenses/>.
*/

package x653.all_in_gold;
import android.graphics.Color;

// FIELD beschreibt die Zielauflage für Feldbogensport

enum FIELD implements Target {

    FIELD_80,FIELD_60,FIELD_40,FIELD_20;

    public int getTargetColor(int points){
        if (points==5) {
                return 0xFFFBD400;
            } else {
                return Color.BLACK;
            }
    }

    public int getBorderColor(int points){
            if (points==5) {
                return Color.BLACK;
            } else {
                return 0xFFFBD400;
            }
    }

    public int maxPoints(){
        return 5;
    }
    public int minPoints(){
        return 1;
    }

    public float getDiameter(){
        switch (this){
         case FIELD_80:
                return 80;
         case FIELD_60:
                return 60;
          case FIELD_40:
                return 40;
             case FIELD_20:
                return 20;
        }
        return 0;
    }

    public float getDiameter(int n){
        return getDiameter()*(maxPoints()+1-n)/(maxPoints()-minPoints()+1);
    }

    public Score getScore(Punkt arrow){
        boolean isX=false;
        float r=arrow.getR();
        int score = Math.max(0,maxPoints()-
                (int)Math.floor(r*2*(maxPoints()+1-minPoints())/getDiameter()));
        if (score<minPoints()) score=0;
        if (r<getDiameter(maxPoints())/4){
            isX=true;
            score++;
        }
        return new Score(score,isX);
    }


    public String toString(){
        return "FIELD " +
                Math.round(getDiameter());
    }

}
