/*
all_in_gold, the master of archery sight calibration.
Copyright (C) 2019 Michael Schröder (mi.schroeder@netcologne.de)

This file is part of all_in_gold.

all_in_gold is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

all_in_gold is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with all_in_gold.  If not, see <http://www.gnu.org/licenses/>.
*/


package x653.all_in_gold;
import java.util.Observable;

//  Distance verwaltet die Entfernung

class Distance extends Observable{
    private int length;
    private Length_Unit unit;

    public Distance() {
        super();
        unit = Length_Unit.METER;
        length = 0;
    }


    public float getCM(){
        return length*unit.getLength();
    }
    public int getLength(){
        return length;
    }
    public Length_Unit getUnit(){
        return unit;
    }

    public void inc(){
        setLength(length+1);
    }

    public void changeUnit(Length_Unit length_unit){
        if (unit!=length_unit){
            setLength(Math.round(this.unit.getLength()*length/length_unit.getLength()));
            unit = length_unit;
            setChanged();
            notifyObservers(unit);
        }
    }

    public void dec(){
        setLength(length-1);
    }

    public void setLength(int l){
        if (length != l && l>=0){
            length = l;
            setChanged();
            notifyObservers(length);
        }
    }

}
