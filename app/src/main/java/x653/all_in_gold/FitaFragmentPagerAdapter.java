/*
all_in_gold, the master of archery sight calibration.
Copyright (C) 2019 Michael Schröder (mi.schroeder@netcologne.de)

This file is part of all_in_gold.

all_in_gold is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

all_in_gold is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with all_in_gold.  If not, see <http://www.gnu.org/licenses/>.
*/

package x653.all_in_gold;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

//  FitaFragmentPagerAdapter verwaltet die Seiten des Pagers.

class FitaFragmentPagerAdapter extends FragmentStatePagerAdapter {
    private Target[] targets;

    FitaFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    void setTargets(Target[] targets){
        this.targets=targets;
    }

    @Override
    public Fragment getItem(int i) {

        // Bundle args = new Bundle();
        // Our object is just an integer :-P
        //args.putSerializable("WTF",targets[i]);
        //fragment.setArguments(args);
        return FitaFragment.newInstance(targets[i]);
    }

    @Override
    public int getCount() {
        if (targets==null) return 0;
        return targets.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return targets[position].toString();
    }

}
