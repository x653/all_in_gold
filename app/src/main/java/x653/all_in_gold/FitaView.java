/*
all_in_gold, the master of archery sight calibration.
Copyright (C) 2019 Michael Schröder (mi.schroeder@netcologne.de)

This file is part of all_in_gold.

all_in_gold is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

all_in_gold is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with all_in_gold.  If not, see <http://www.gnu.org/licenses/>.
*/

package x653.all_in_gold;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;

//  View eines Targets

public class FitaView extends android.support.v7.widget.AppCompatImageView {

    private Target scheibe;
    private float diameter_px;
    private final Paint drawFita;
    private final Paint drawFitaBorder;

    public FitaView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setWillNotDraw(false);
        scheibe = FITA.FITA_122;
        drawFita = new Paint(Paint.DITHER_FLAG);
        drawFitaBorder = new Paint(Paint.DITHER_FLAG);
    }

    public void setScheibe(Target s){
        scheibe=s;
        this.invalidate();
    }

    @Override
    protected void onSizeChanged(int w, int h, int width, int height) {
            super.onSizeChanged(w, h, width, height);
        super.onSizeChanged(w, h, width, height);
        width=w;
        height=h;
        if (width>=height)
            diameter_px = (float) (0.8 * height);
        else
            diameter_px = (float) (0.8 * width);
        }


        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            drawFita.setAntiAlias(true);
            drawFita.setStyle(Paint.Style.FILL);
            drawFita.setStrokeWidth(0);
            drawFitaBorder.setAntiAlias(true);
            drawFitaBorder.setStyle(Paint.Style.STROKE);
            drawFitaBorder.setStrokeWidth(2);
            float radius=0;
            for (int i=scheibe.minPoints();i<=scheibe.maxPoints();i++){
                drawFita.setColor(scheibe.getTargetColor(i));
                drawFitaBorder.setColor(scheibe.getBorderColor(i));
                radius=scheibe.getDiameter(i)/scheibe.getDiameter()*diameter_px/2;
                canvas.drawCircle(getWidth()/2,getHeight()/2,
                        radius,drawFita);
                canvas.drawCircle(getWidth()/2,getHeight()/2,
                        radius,drawFitaBorder);
            }
            canvas.drawCircle(getWidth()/2,getHeight()/2,
                    radius/2,drawFitaBorder);
        }

}
