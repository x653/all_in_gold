/*
all_in_gold, the master of archery sight calibration.
Copyright (C) 2019 Michael Schröder (mi.schroeder@netcologne.de)

This file is part of all_in_gold.

all_in_gold is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

all_in_gold is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with all_in_gold.  If not, see <http://www.gnu.org/licenses/>.
*/

package x653.all_in_gold;
import android.graphics.Color;

// FITA beschreibt die Zielauflagen der FITA

enum FITA implements Target {
    FITA_122,FITA_80,FITA_60,FITA_40,
    SPOT_40,SPOT_30,SPOT_20;

    boolean isSpot(){
        return (this==SPOT_20 || this==SPOT_30 ||this==SPOT_40);
    }

    public int getTargetColor(int points){
        switch (points){
            case 1: case 2:
                return Color.WHITE;
            case 3: case 4:
                return 0xFF131313;
            case 5: case 6:
                return 0xFF009CCC;
            case 7: case 8:
                return 0xFFD91919;
            case 9: case 10:
                return 0xFFFBD400;
        }
        return Color.BLACK;
    }

    public int getBorderColor(int points){
        if (points==3 || points ==4)
                return Color.WHITE;
            else
                return Color.BLACK;
    }

    public int maxPoints(){
        return 10;
    }

    public int minPoints(){
        if (isSpot()) return 6;
        return 1;
    }

    public float getDiameter(){
        switch (this){
            case FITA_122:
                return 122;
            case FITA_80:
                return 80;
            case FITA_60:
                return 60;
            case FITA_40: case SPOT_40:
                return 40;
            case SPOT_30:
                return 30;
            case SPOT_20:
                return 20;
        }
        return 0;
    }

    public float getDiameter(int n){
        return getDiameter()*(maxPoints()+1-n)/(maxPoints()-minPoints()+1);
    }
    public Score getScore(Punkt arrow){
        boolean isX=false;
        float r=arrow.getR();
        int score = Math.max(0,maxPoints()-
                (int)Math.floor(r*2*(maxPoints()+1-minPoints())/getDiameter()));
        if (score<minPoints()) score=0;
        if (r<getDiameter(maxPoints())/4) isX=true;
        return new Score(score,isX);
    }

    public String toString(){
        StringBuilder stringBuilder=new StringBuilder();
        stringBuilder.append("FITA ");
        if (isSpot()) stringBuilder.append("SPOT ");
        stringBuilder.append((int)getDiameter());
        return stringBuilder.toString();
    }

}
