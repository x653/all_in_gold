/*
all_in_gold, the master of archery sight calibration.
Copyright (C) 2019 Michael Schröder (mi.schroeder@netcologne.de)

This file is part of all_in_gold.

all_in_gold is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

all_in_gold is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with all_in_gold.  If not, see <http://www.gnu.org/licenses/>.
*/

package x653.all_in_gold;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import java.util.ArrayList;

// ArrowView malt Pfeile

public class ArrowView extends android.support.v7.widget.AppCompatImageView  {
    private float x;                // in px
    private float y;                // in px
    private float diameter_px;      // in px
    private final Paint drawArrow;
    private final Paint drawPasse;
    private ArrayList<Arrow> arrows;
    private Target target;
    private ArrayList<Arrow> passe;
    private boolean enabled;

    public ArrowView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setWillNotDraw(false);
        drawArrow = new Paint(Paint.DITHER_FLAG);
        drawArrow.setAntiAlias(true);
        drawArrow.setColor(ContextCompat.getColor(context,R.color.colorArrows));
        drawArrow.setStyle(Paint.Style.FILL);
        drawArrow.setStrokeWidth(1);
        drawPasse = new Paint(Paint.DITHER_FLAG);
        drawPasse.setAntiAlias(true);
        drawPasse.setColor(ContextCompat.getColor(context,R.color.colorLastArrows));
        drawPasse.setColor(Color.MAGENTA);
        drawPasse.setStyle(Paint.Style.FILL);
        drawPasse.setStrokeWidth(1);
        arrows=null;
        passe = null;
        enabled=false;
        x=-1;
        y=-1;
    }

    public void setTarget(Target target){
        this.target=target;
        invalidate();
    }
    public void setEnabled(boolean enabled){
        this.enabled=enabled;
        super.setEnabled(enabled);
    }

    public void setPasse(ArrayList<Arrow> passe){
        this.passe = passe;
        invalidate();
    }
    public void setArrows(ArrayList<Arrow> arrows){
        this.arrows = arrows;
        invalidate();
    }

    @Override
    protected void onSizeChanged(int w, int h, int width, int height) {
        super.onSizeChanged(w, h, width, height);
        width=w;
        height=h;
        if (width>=height)
            diameter_px = ((float)0.8 * height);
        else
            diameter_px = ((float)0.8 * width);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (arrows != null) {
            for (Arrow arrow : arrows) {
                canvas.drawCircle(getWidth() / 2 +
                                diameter_px * arrow.getX() / target.getDiameter(),
                        getHeight() / 2 +
                                diameter_px * arrow.getY() / target.getDiameter(),
                        diameter_px* Arrow.getRadius()/target.getDiameter(), drawArrow);
            }
        }
        if (passe != null) {
            for (Arrow arrow : passe) {
                canvas.drawCircle(getWidth() / 2 +
                                diameter_px * arrow.getX() / target.getDiameter(),
                        getHeight() / 2 +
                                diameter_px * arrow.getY() / target.getDiameter(),
                        diameter_px* Arrow.getRadius()/target.getDiameter(), drawPasse);
            }


        }
        if (x>=0 && y>=0) canvas.drawCircle(x, y, diameter_px*Arrow.getRadius()/target.getDiameter(), drawPasse);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (passe!=null && enabled){
            switch (event.getAction()){
                case MotionEvent.ACTION_DOWN:
                    x = event.getX();
                    y = event.getY()-100;
                    break;
                case MotionEvent.ACTION_UP:
                    performClick();
                    break;
                case MotionEvent.ACTION_MOVE:
                    x = event.getX();
                    y = event.getY()-100;
                    break;
                default:
                    return true;
            }
            invalidate();
            return true;
        }
        return false;
    }

    Punkt getArrow(){
        Punkt p=new Punkt(
                (x-(float)getWidth()/2)/ diameter_px *target.getDiameter(),
                (y-(float)getHeight()/2)/ diameter_px *target.getDiameter());
        x=-1;
        y=-1;
        return p;
    }

    @Override
    public boolean performClick(){
        if (passe!=null && enabled) {
            return super.performClick();
        }
        return false;
    }

}