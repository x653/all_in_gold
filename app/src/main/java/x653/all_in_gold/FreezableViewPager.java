/*
all_in_gold, the master of archery sight calibration.
Copyright (C) 2019 Michael Schröder (mi.schroeder@netcologne.de)

This file is part of all_in_gold.

all_in_gold is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

all_in_gold is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with all_in_gold.  If not, see <http://www.gnu.org/licenses/>.
*/

package x653.all_in_gold;
import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

//  Ein ViewPager den man an- und ausschalten kann.

public class FreezableViewPager extends ViewPager {
    private boolean enabled;

    public FreezableViewPager(@NonNull Context context) {
        super(context);
        enabled=true;
    }

    public FreezableViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        enabled=true;
    }

    @SuppressLint("ClickableViewAccessibility")
    public boolean onTouchEvent(MotionEvent ev){
        return (enabled&&super.onTouchEvent(ev));
    }
    public boolean performClick(){
        return (enabled&&super.performClick());
    }
    public boolean onInterceptTouchEvent(MotionEvent ev){
        return (enabled && super.onInterceptTouchEvent(ev));
    }
    public void setEnabled(boolean pEnabled){
        enabled=pEnabled;
    }

}
