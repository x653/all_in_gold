/*
all_in_gold, the master of archery sight calibration.
Copyright (C) 2019 Michael Schröder (mi.schroeder@netcologne.de)

This file is part of all_in_gold.

all_in_gold is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

all_in_gold is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with all_in_gold.  If not, see <http://www.gnu.org/licenses/>.
*/

package x653.all_in_gold;
import android.annotation.SuppressLint;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import me.relex.circleindicator.CircleIndicator;

//  AllInGoldView verwaltet die gesamte View der App.

class AllInGoldView {
    private final FreezableViewPager viewPager;
    private final FitaFragmentPagerAdapter fitaFragmentPagerAdapter;
    private final CircleIndicator circleIndicator;
    private final ArrowView arrowView;
    private final FloatingActionButton floatingActionButton;
    private final TextView textViewArrowAverage;
    private final TextView textViewPasseAverage;
    private final TextView textViewPasseScore;
    private final TextView textViewElevation;
    private final Switch theSwitch;
    private final android.support.v7.widget.AppCompatImageButton buttonElevationPlus;
    private final android.support.v7.widget.AppCompatImageButton buttonElevationMinus;
    private final TextView textViewWindage;
    private final TextView textViewWindageUnit;
    private final android.support.v7.widget.AppCompatImageButton buttonWindagePlus;
    private final android.support.v7.widget.AppCompatImageButton buttonWindageMinus;
    private final TextView textViewDistance;
    private final TextView textViewDistanceUnit;
    private final android.support.v7.widget.AppCompatImageButton buttonDistancePlus;
    private final android.support.v7.widget.AppCompatImageButton buttonDistanceMinus;
    private final SeekBar seekBar;
    private Snackbar snackbar;

    public AllInGoldView(AppCompatActivity main){
        viewPager = main.findViewById(R.id.pager2);
        circleIndicator = main.findViewById(R.id.indicator);
        circleIndicator.setViewPager(viewPager);
        arrowView = main.findViewById(R.id.touchView2);
        fitaFragmentPagerAdapter =
                new FitaFragmentPagerAdapter(main.getSupportFragmentManager());
        viewPager.setAdapter(fitaFragmentPagerAdapter);
        floatingActionButton=main.findViewById(R.id.fab);
        textViewPasseAverage=main.findViewById(R.id.textAVG2);
        textViewArrowAverage=main.findViewById(R.id.textAVG1);
        textViewPasseScore=main.findViewById(R.id.textLast);
        theSwitch = main.findViewById(R.id.switch1);
        textViewElevation = main.findViewById(R.id.textViewElevation);
        buttonElevationPlus = main.findViewById(R.id.button_Elevation_Plus);
        buttonElevationMinus = main.findViewById(R.id.button_Elevation_Minus);
        textViewWindage = main.findViewById(R.id.textViewWindage);
        textViewWindageUnit = main.findViewById(R.id.textViewWindageUnit);
        buttonWindagePlus = main.findViewById(R.id.button_Windage_Plus);
        buttonWindageMinus = main.findViewById(R.id.button_Windage_Minus);
        textViewDistance = main.findViewById(R.id.textViewDistance);
        textViewDistanceUnit = main.findViewById(R.id.textViewDistanceUnit);
        buttonDistancePlus=main.findViewById(R.id.button_Distance_Plus);
        buttonDistanceMinus=main.findViewById(R.id.button_Distance_Minus);
        seekBar = main.findViewById(R.id.seekBar2);
    }

    void setTarget(Target[] targets){
        fitaFragmentPagerAdapter.setTargets(targets);
        viewPager.setAdapter(fitaFragmentPagerAdapter);
        circleIndicator.setViewPager(viewPager);
        setTarget(targets[0]);
    }
    void setTarget(Target target){
        arrowView.setTarget(target);
        viewPager.setCurrentItem(target.ordinal());
    }
    void addTargetListener(ViewPager.OnPageChangeListener onPageChangeListener){
        viewPager.addOnPageChangeListener(onPageChangeListener);
    }
    void setArrows(ArrayList<Arrow> arrows){
        arrowView.setArrows(arrows);
    }
    void setPasse(ArrayList<Arrow> arrows){
        arrowView.setPasse(arrows);
    }

    void enableArrowsEditor(){
        viewPager.setEnabled(false);
        snackbar = Snackbar.make(floatingActionButton, "Tap on Target to Enter Arrows", Snackbar.LENGTH_INDEFINITE);
        snackbar.show();
        buttonElevationPlus.setEnabled(false);
        buttonElevationMinus.setEnabled(false);
        buttonDistancePlus.setEnabled(false);
        buttonDistanceMinus.setEnabled(false);
        buttonWindageMinus.setEnabled(false);
        buttonWindagePlus.setEnabled(false);
        seekBar.setEnabled(false);
        arrowView.setEnabled(true);
        floatingActionButton.setImageResource(R.mipmap.baseline_done_white_24);
    }
    void doneArrowsEditor(){
        viewPager.setEnabled(true);
        if (snackbar!=null) snackbar.dismiss();
        arrowView.setEnabled(false);
        buttonElevationPlus.setEnabled(true);
        buttonElevationMinus.setEnabled(true);
        buttonDistancePlus.setEnabled(true);
        buttonDistanceMinus.setEnabled(true);
        buttonWindageMinus.setEnabled(true);
        buttonWindagePlus.setEnabled(true);
        seekBar.setEnabled(true);
        floatingActionButton.setImageResource(R.mipmap.baseline_add_white_24);
    }
    void setFloatingActionButtonListener(View.OnClickListener onClickListener){
        floatingActionButton.setOnClickListener(onClickListener);
    }
    void setArrowListener(View.OnClickListener onClickListener){
        arrowView.setOnClickListener(onClickListener);
    }
    void setPasseAverage(String s){
        textViewPasseAverage.setText(s);
    }
    void setArrowAverage(String s){
        textViewArrowAverage.setText(s);
    }
    void setPasseScore(String s){
        textViewPasseScore.setText(s);
    }

    @SuppressLint("ClickableViewAccessibility")
    void setListenerElevationPlus(View.OnTouchListener onClickListener){
        buttonElevationPlus.setOnTouchListener(onClickListener);
    }
    @SuppressLint("ClickableViewAccessibility")
    void setListenerElevationMinus(View.OnTouchListener onClickListener){
        buttonElevationMinus.setOnTouchListener(onClickListener);
    }
    void setListenerWindagePlus(View.OnClickListener onClickListener){
        buttonWindagePlus.setOnClickListener(onClickListener);
    }
    void setListenerWindageMinus(View.OnClickListener onClickListener){
        buttonWindageMinus.setOnClickListener(onClickListener);
    }
    void setElevation(String s){
        textViewElevation.setText(s);
    }
    void setWindage(String s) {
        textViewWindage.setText(s);
    }
    void setWindageUnit(String s) {
        textViewWindageUnit.setText(s);
    }

    void setDistance(String s){
        textViewDistanceUnit.setText(String.format("[%s]", s));
    }
    void setDistance(Integer progess){
        textViewDistance.setText(Integer.toString(progess));
        seekBar.setProgress(progess);
    }
    void setListenerDistancePlus(View.OnClickListener onClickListener){
        buttonDistancePlus.setOnClickListener(onClickListener);
    }
    void setListenerDistanceMinus(View.OnClickListener onClickListener){
        buttonDistanceMinus.setOnClickListener(onClickListener);
    }
    void setSeekBarListener(SeekBar.OnSeekBarChangeListener onSeekBarChangeListener){
        seekBar.setOnSeekBarChangeListener(onSeekBarChangeListener);
    }
    void addSwitchListener(View.OnClickListener onClickListener){
        theSwitch.setOnClickListener(onClickListener);
    }
    void setTheSwitch(boolean akt){
        theSwitch.setChecked(akt);
    }
    void updateArrows(){
        arrowView.invalidate();
    }


}