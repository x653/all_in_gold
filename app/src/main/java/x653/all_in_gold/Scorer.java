/*
all_in_gold, the master of archery sight calibration.
Copyright (C) 2019 Michael Schröder (mi.schroeder@netcologne.de)

This file is part of all_in_gold.

all_in_gold is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

all_in_gold is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with all_in_gold.  If not, see <http://www.gnu.org/licenses/>.
*/

package x653.all_in_gold;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

// Scorer kann Pfeiltreffer auf der Zielauflage bewerten

class Scorer {

    static String getPoints(Target target, ArrayList<Arrow> arrows){
        if (arrows==null) return "-";
        ArrayList<Score> pkte=new ArrayList<>();
        for (Arrow arrow:arrows) {
            pkte.add(target.getScore(arrow));
        }
        Collections.sort(pkte, new Comparator<Score>() {
            @Override
            public int compare(Score o1, Score o2) {
                if (o1.isX() && o2.isX()) return  0;
                if (o1.isX()) return -1;
                if (o2.isX()) return 1;
                return o2.getPoints()-o1.getPoints();
            }
        });

        StringBuilder stringBuilder = new StringBuilder();
        for (Score score:pkte) {
            if (stringBuilder.length()!=0) stringBuilder.append(" - ");
            stringBuilder.append(score.toString());
        }
        return  stringBuilder.toString();
    }

    static float getAverage(Target target, ArrayList<Arrow> arrows){
        if (target==null) return Float.NaN;
        if (arrows==null) return Float.NaN;
        if (arrows.size()==0) return Float.NaN;
        float sum=0;
        for (Arrow arrow:arrows){
            sum += target.getScore(arrow).getPoints();
        }
        return sum/arrows.size();
    }

}