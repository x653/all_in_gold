/*
all_in_gold, the master of archery sight calibration.
Copyright (C) 2019 Michael Schröder (mi.schroeder@netcologne.de)

This file is part of all_in_gold.

all_in_gold is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

all_in_gold is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with all_in_gold.  If not, see <http://www.gnu.org/licenses/>.
*/

package x653.all_in_gold;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

//  FitaFragment ist View einer Zielauflage

@SuppressLint("ValidFragment")
public class FitaFragment extends Fragment {
    private Target target;


    // newInstance constructor for creating fragment with arguments
    public static FitaFragment newInstance(Target target) {
        FitaFragment fragmentFita = new FitaFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", target.ordinal());
        args.putBoolean("someTitle", target.getClass()==FITA.class);
        fragmentFita.setArguments(args);
        return fragmentFita;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        boolean isFita=getArguments().getBoolean("someTitle");
        if (isFita) target=FITA.values()[getArguments().getInt("someInt", 0)];
        else target=FIELD.values()[getArguments().getInt("someInt", 0)];
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.scheibe, container, false);
        if (target!=null) {
            ((TextView) view.findViewById(R.id.Fita_Titel)).setText(
                    target.toString());
            ((FitaView) view.findViewById(R.id.scheibe)).setScheibe(target);
        }
        return view;
    }

}
