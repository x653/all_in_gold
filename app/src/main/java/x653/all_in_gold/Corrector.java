/*
all_in_gold, the master of archery sight calibration.
Copyright (C) 2019 Michael Schröder (mi.schroeder@netcologne.de)

This file is part of all_in_gold.

all_in_gold is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

all_in_gold is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with all_in_gold.  If not, see <http://www.gnu.org/licenses/>.
*/

package x653.all_in_gold;
import java.util.ArrayList;

// Corrector korrigiert Pfeiltreffer mit Geometrie und Regression

public class Corrector {
    private static float eye=(float)100;    // in cm
    private static Regression regression = new Regression();

    enum QUALITY{
        NO_FIX,HAS_FIX,IS_FIX
    }

    static QUALITY getQuality(float distance,float sight){
        if (!regression.isQuadratic()) return QUALITY.NO_FIX;
        if (Math.abs(sight-regression.quad(distance))<0.1) return QUALITY.IS_FIX;
        return QUALITY.HAS_FIX;
    }

    static float getEye(){
        return eye;
    }
    static void setEye(float pEye){
        eye=pEye;
    }
    static void newRegression(){regression=new Regression(); }
    private static void addArrow(Arrow arrow){
        regression.add(arrow.getDistance(),bestSight(arrow));
    }
    static void addArrows(ArrayList<Arrow> arrows){
        for (Arrow arrow:arrows) addArrow(arrow);
    }
    static float bestSight(float distance){return (float)regression.quad(distance);}
    private static float bestSight(Arrow arrow) {
        return arrow.getElevation() + arrow.getY() * eye / arrow.getDistance();
    }
    private static Arrow changeSight(Arrow arrow, float newSight){
        float distance=arrow.getDistance();
        return new Arrow(distance,newSight,
                arrow.getX(), arrow.getY()+(arrow.getElevation()-newSight)*distance/eye);
    }
    private static Arrow changeSightWindage(Arrow arrow, float newSight, float windage){
        float distance=arrow.getDistance();
        return new Arrow(distance,newSight,
                arrow.getX()+windage*distance/eye, arrow.getY()+(arrow.getElevation()-newSight)*distance/eye);
    }
    private static Arrow changeDistance(Arrow arrow, float newDistance, float newSight){
        float scale=newDistance/arrow.getDistance();
        return new Arrow(newDistance,newSight,
                arrow.getX()*scale,arrow.getY()*scale);
    }

    static ArrayList<Arrow> changeAll(ArrayList<Arrow> arrows,
                                      float newDistance, float newSight, float windage){
        ArrayList<Arrow> list= new ArrayList<>();
        for (Arrow arrow:arrows){
            Arrow atRegression=changeSightWindage(arrow,(float)regression.quad(arrow.getDistance()),windage);
            Arrow atNewDistance=changeDistance(atRegression,newDistance,(float)regression.quad(newDistance));
            Arrow atNewSight=changeSight(atNewDistance,newSight);
            list.add(atNewSight);
        }
        return list;
    }

}
