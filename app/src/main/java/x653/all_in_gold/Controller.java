/*
all_in_gold, the master of archery sight calibration.
Copyright (C) 2019 Michael Schröder (mi.schroeder@netcologne.de)

This file is part of all_in_gold.

all_in_gold is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

all_in_gold is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with all_in_gold.  If not, see <http://www.gnu.org/licenses/>.
*/

package x653.all_in_gold;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.Toast;

import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

//  Controller vermittelt zwischen Model und View

class Controller {
    private final Model model;
    private final AllInGoldView allInGoldView;
    private final String isFITA="isFITA";
    private final String TargetNumber="TargetNumber";
    private final String Elevation="Elevation";
    private final String Distance="Distance";
    private final String Windage="Windage";
    private final String ElevationUnit="ElevationUnit";
    private final String DistanceUnit="DistanceUnit";
    private final String Eye="Eye";
    private final String AutoCalibrator="AutoCalibrator";

    public Controller(AppCompatActivity main) {
        model = new Model();
        allInGoldView = new AllInGoldView(main);

        model.addTargetObserver(new Observer() {
            @Override
            public void update(Observable o, Object arg) {
            Log(o,arg);
            if (arg.getClass().isArray()){
                allInGoldView.setTarget((Target[])arg);
            } else {
                allInGoldView.setTarget((Target)arg);
            }
            updateArrow();
            }
        });

        allInGoldView.addTargetListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                model.setTarget(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        allInGoldView.setFloatingActionButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                model.setWindage(0);model.action();
            }
        });

        allInGoldView.setArrowListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrowView arrowView=(ArrowView)v;
                model.addArrow(arrowView.getArrow());
            }
        });


        model.addArrowsObserver(new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                Log(o,arg);
                if (arg!=null && arg.getClass()==Boolean.class) {
                    if (!(boolean) arg) {
                        allInGoldView.setArrows(model.getArrows());
                        allInGoldView.setPasse(model.getLast());
                        allInGoldView.doneArrowsEditor();
                    }
                    else {
                        allInGoldView.setPasse(model.getPasse());
                        allInGoldView.setArrows(null);
                        allInGoldView.enableArrowsEditor();
                    }
                }
                updateArrow();
            }
        });

        model.addElevationObserver(new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                Log(o,arg);
                if (arg.getClass()==String.class) {
                    allInGoldView.setElevation((String) arg);
                    allInGoldView.setArrows(model.getArrows());
                    allInGoldView.setPasse(model.getLast());
                    updateArrow();
                }
                else if (arg.getClass()== SightSetting.UNIT.class);
                    //keine Maßeinheit bei Elevation nötig
            }
        });
        allInGoldView.setListenerElevationMinus(new RepeatListener(400, 100, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                model.elevationDec();
            }
        }));

        allInGoldView.setListenerElevationPlus(new RepeatListener(400, 100, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                model.elevationInc();
            }
        }));

        allInGoldView.addSwitchListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                model.setAutoCalibrator(((Switch)v).isChecked());
            }
        });

        model.addWindageObserver(new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                Log(o,arg);
                if (arg.getClass()==String.class) {
                    allInGoldView.setWindage((String) arg);
                    allInGoldView.setArrows(model.getArrows());
                    allInGoldView.setPasse(model.getLast());
                    updateArrow();
                }
                if (arg.getClass()== SightSetting.UNIT.class){
                    allInGoldView.setWindageUnit(((SightSetting.UNIT)arg).toString());
                }
            }
        });
        allInGoldView.setListenerWindageMinus(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                model.windageDec();
            }
        });
        allInGoldView.setListenerWindagePlus(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                model.windageInc();
            }
        });


        model.addDistanceObserver(new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                Log(o,arg);
                if (arg.getClass()==Integer.class) {
                    allInGoldView.setDistance((Integer)arg);
                    allInGoldView.setArrows(model.getArrows());
                    allInGoldView.setPasse(model.getLast());
                    updateArrow();
                }
                if (arg.getClass()==Length_Unit.class)
                    allInGoldView.setDistance(((Length_Unit)arg).toString());
            }
        });
        allInGoldView.setListenerDistancePlus(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                model.distanceInc();
            }
        });
        allInGoldView.setListenerDistanceMinus(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                model.distanceDec();
            }
        });
        allInGoldView.setSeekBarListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                model.setDistance(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        reset();
        load(main);
    }

    void reset(){
        model.setFIELD();
        model.setTarget(1);
        model.setElevationUnit(SightSetting.UNIT.UNIT_20MM);
        model.setAutoCalibrator(true);
        model.setElevation((float)2.9);
        model.setDistance(20);
        model.setWindage(0);
    }
    private String floatToString(float a){
        if (Float.isNaN(a)) return "-";
        return String.format(Locale.getDefault(),"%.1f",a);
    }


    private void updateArrow(){
        allInGoldView.setPasseAverage(floatToString(model.getAveragePasse()));
        allInGoldView.setArrowAverage(floatToString(model.getAverage()));
        allInGoldView.setPasseScore(model.getPasseScore());
        allInGoldView.updateArrows();
    }

    private void load(AppCompatActivity main){
        model.load(FileIO.load(main));
    }
    public void save(AppCompatActivity main){
        FileIO.save(main,model.save());
    }
    void loadPreferences(SharedPreferences sharedPreferences){
        if (sharedPreferences.contains(isFITA)) {
            boolean isfita=sharedPreferences.getBoolean(isFITA, true);
            if (isfita) model.setFITA();
            else model.setFIELD();
        }
        if (sharedPreferences.contains(TargetNumber)) {
            int ta=sharedPreferences.getInt(TargetNumber, 1);
            model.setTarget(ta);
        }
        if (sharedPreferences.contains(DistanceUnit)) {
            int ta=sharedPreferences.getInt(DistanceUnit, 0);
            model.setDistanceUnit(Length_Unit.values()[ta]);
        }
        if (sharedPreferences.contains(Distance)) {
            int ta=sharedPreferences.getInt(Distance, 1);
            model.setDistance(ta);
        }
        if (sharedPreferences.contains(ElevationUnit)) {
            int ta=sharedPreferences.getInt(ElevationUnit, 1);
            model.setElevationUnit(SightSetting.UNIT.values()[ta]);
        }
        if (sharedPreferences.contains(Elevation)) {
            float ta=sharedPreferences.getFloat(Elevation, (float)2.9);
            model.setElevation(ta);
        }
        if (sharedPreferences.contains(Windage)) {
            float ta=sharedPreferences.getFloat(Windage, 0);
            model.setWindage(ta);
        }
        if (sharedPreferences.contains(Eye)) {
            float ta=sharedPreferences.getFloat(Eye, (float)100);
            model.setEye(ta);
        }
        if (sharedPreferences.contains(AutoCalibrator)) {
            boolean auto=sharedPreferences.getBoolean(AutoCalibrator, true);
            model.setAutoCalibrator(auto);
        }

    }
    void savePreferences(SharedPreferences.Editor editor){
        editor.putBoolean(isFITA,model.isFITA());
        editor.putInt(TargetNumber, model.getTarget().ordinal());
        editor.putInt(Distance, model.getDistanceLength());
        editor.putFloat(Elevation, model.getElevationValue());
        editor.putInt(ElevationUnit, model.getElevationUnit().ordinal());
        editor.putInt(DistanceUnit, model.getDistanceUnit().ordinal());
        editor.putFloat(Windage, model.getWindageValue());
        editor.putFloat(Eye, model.getEye());
        editor.putBoolean(AutoCalibrator, model.getAutoCalibrator());
        editor.commit();
    }

    public void delete(){
        model.deleteAll();
    }
    public void deleteLAST(){
        model.deleteLast();
    }

    public boolean undoo(){
        return model.undoo();
    }

    public DialogInterface.OnClickListener reloadEye(View view){
        final EditText eye = view.findViewById(R.id.editTextEye);
        final RadioButton radioButton10mm = view.findViewById(R.id.radioButton10mm);
        final RadioButton radioButton20mm = view.findViewById(R.id.radioButton20mm);

        eye.setHint(Float.toString(model.getEye()));
        switch (model.getElevationUnit()) {
            case UNIT_10MM:
                radioButton10mm.setChecked(true);
                break;
            case UNIT_20MM:
                radioButton20mm.setChecked(true);
                break;
        }


        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    model.setEye(Float.parseFloat(eye.getText().toString()));
                } catch (Exception ignored){

                }
                if (radioButton10mm.isChecked()) model.setElevationUnit(SightSetting.UNIT.UNIT_10MM);
                if (radioButton20mm.isChecked()) model.setElevationUnit(SightSetting.UNIT.UNIT_20MM);
            }
        };
    }



    public void doSettings(View view){
        RadioButton radioButtonMeter = view.findViewById(R.id.radioButtonMeter);
        RadioButton radioButtonYard = view.findViewById(R.id.radioButtonYard);
        RadioButton radioButtonStep = view.findViewById(R.id.radioButtonStep);
        switch (model.getDistanceUnit()){
            case METER:
                radioButtonMeter.setChecked(true);
                break;
            case YARD:
                radioButtonYard.setChecked(true);
                break;
            case STEP:
                radioButtonStep.setChecked(true);
                break;
        }
        radioButtonMeter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                model.setDistanceUnit(Length_Unit.METER);
            }
        });
        radioButtonYard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                model.setDistanceUnit(Length_Unit.YARD);
            }
        });
        radioButtonStep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                model.setDistanceUnit(Length_Unit.STEP);
            }
        });

        RadioButton radioButtonFIELD = view.findViewById(R.id.radioButtonFIELD);
        RadioButton radioButtonFITA = view.findViewById(R.id.radioButtonFITA);
        if (model.isFIELD())
            radioButtonFIELD.setChecked(true);
        else
            radioButtonFITA.setChecked(true);

        radioButtonFITA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { model.setFITA();
            }
        });
        radioButtonFIELD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { model.setFIELD();
            }
        });
    }

    private void Log(Object o, Object arg){
        Log.d(Global.LOG,"Message: "+o.toString()+" arg: "+arg);
    }
    void addMenuIcon(final MainActivity main, final MenuItem item){
        model.addAutoCalibratorObserver(new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                Log(o,arg);
                if (arg.getClass()==Boolean.class){
                    allInGoldView.setTheSwitch((boolean)arg);
                }

                if (arg.getClass()== Corrector.QUALITY.class){
                    switch ((Corrector.QUALITY)arg){
                        case NO_FIX:item.setIcon(R.mipmap.baseline_gps_off_white_24);
//                        main.makeToast("Enter arrows from at least three distances");
                        break;
                        case HAS_FIX:item.setIcon(R.mipmap.baseline_gps_not_fixed_white_24);break;
                        case IS_FIX:item.setIcon(R.mipmap.baseline_gps_fixed_white_24);break;
                    }
                }
            }
        });
    }

}