/*
all_in_gold, the master of archery sight calibration.
Copyright (C) 2019 Michael Schröder (mi.schroeder@netcologne.de)

This file is part of all_in_gold.

all_in_gold is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

all_in_gold is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with all_in_gold.  If not, see <http://www.gnu.org/licenses/>.
*/

package x653.all_in_gold;

// Regression führt eine Quadratische Regression durch

import android.util.Log;

class Regression {
    private double sx;
    private double sy;
    private double sx2;
    private double sxy;
    private double sx2y;
    private double sx3;
    private double sx4;
    private double n;
    private double a;
    private double b;
    private double c;
    private double d1;
    private double d2;
    private int d3;


    Regression() {
        n = 0;
        sx = 0;
        sy = 0;
        sxy = 0;
        sx2y = 0;
        sx2 = 0;
        sx3 = 0;
        sx4 = 0;
        a=0;
        b=0;
        c=0;
        d1=Double.MAX_VALUE;
        d2=Double.MIN_VALUE;
        d3=-1;
    }

    void add(double x, double y) {
        Log.d(Global.LOG,String.format("Regression dist,bestsight: %f %f",x,y));
        sx += x;
        sy += y;
        sx2 += x * x;
        sxy += x * y;
        sx2y += x * x * y;
        sx3 += x * x * x;
        sx4 += x * x * x * x;
        n+=1;
        if (x<d1){
            d1=x;
            d3++;
        }
        if (x>d2){
            d2=x;
            d3++;
        }
        makeQuadratic();
    }
    public boolean isIntervall(double x){return (x>d1)&&(x<d2);}

    public boolean isQuadratic(){
        return (d3>=3);
    }
    public boolean isLinear(){
        return (d3==2);
    }
    public boolean isConstant(){
        return (d3==1);
    }
    public boolean isNull(){return (d3<=0);}

    double getN(){
        return n;
    }
    double getMeanX(){
        if (n!=0)
            return sx/n;
        return 0;
    }

    double getMeanY(){
        if (n!=0)
            return sy/n;
        return 0;
    }

    private void makeLinear() {
        c=0;
        if (d3 > 1) {
            double q = (n * sx2 - sx * sx);
            if (q != 0) {
                b = (n * sxy - sx * sy) / q;
                a = (sy - b * sx) / n;
                return;
            }
        }
        if (d3==1) {
            b = 0;
            a = sy / n;
            return;
        }
        a = 0;
        b = 0;
    }

    double lin(double x) {
        return b * x + a;
    }
    double quad(double x) {
        Log.d(Global.LOG,String.format("Regression: %f %f %f %f = %f",a,b,c,x,c*x*x+b*x+a));
        return c * x * x + b * x + a;
    }

    private void makeQuadratic() {
        //   Quadratic Regression Equation(y) = a x ^ 2 + b x + c
        if (d3>2) {
            double Sxx = sx2 - sx * sx / n;
            double Sxy = sxy - sx * sy / n;
            double Sxx2 = sx3 - sx2 * sx / n;
            double Sx2y = sx2y - sx2 * sy / n;
            double Sx2x2 = sx4 - sx2 * sx2 / n;

            double q=(Sxx * Sx2x2 - Sxx2 * Sxx2);
            if (q!=0) {
                c = (Sx2y * Sxx - Sxy * Sxx2) / q;
                b = (Sxy * Sx2x2 - Sx2y * Sxx2) / q;
                a = (sy / n) - b * sx / n - c * sx2 / n;
                return;
            }
        }
        c = 0;
        makeLinear();
    }

}
