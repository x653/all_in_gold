/*
all_in_gold, the master of archery sight calibration.
Copyright (C) 2019 Michael Schröder (mi.schroeder@netcologne.de)

This file is part of all_in_gold.

all_in_gold is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

all_in_gold is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with all_in_gold.  If not, see <http://www.gnu.org/licenses/>.
*/

package x653.all_in_gold;
import java.util.Observable;

//  Die Klasse Targets verwaltet die Zielauflagen.

class Targets extends Observable{
    enum TARGET_TYPE{
        FITA,FIELD
    }
    private TARGET_TYPE type;
    private Target target;

    Targets(){
        type=TARGET_TYPE.FITA;
    }

    void setFIELD(){
            type=TARGET_TYPE.FIELD;
            setChanged();
            notifyObservers(getTargets());
    }
    void setFITA(){
            type = TARGET_TYPE.FITA;
            setChanged();
            notifyObservers(getTargets());

    }
    boolean isFIELD(){
        return (type==TARGET_TYPE.FIELD);
    }
    boolean isFITA(){
        return (type==TARGET_TYPE.FITA);
    }
    Target[] getTargets(){
        switch (type){
            case FITA:
                return FITA.values();
            case FIELD:
                return FIELD.values();
        }
        return null;
    }

    Target getTarget(){
        return target;
    }
    void setTarget(int index){
        //if (target==null||target.ordinal()!=index) {
            target = getTargets()[index];
            setChanged();
            notifyObservers(target);
        //}
    }

}