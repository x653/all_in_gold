/*
all_in_gold, the master of archery sight calibration.
Copyright (C) 2019 Michael Schröder (mi.schroeder@netcologne.de)

This file is part of all_in_gold.

all_in_gold is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

all_in_gold is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with all_in_gold.  If not, see <http://www.gnu.org/licenses/>.
*/

package x653.all_in_gold;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Locale;

//  MainActivity wird vom System gestartet

public class MainActivity extends AppCompatActivity {
    Toast toast;
    private Controller controller;
    private SharedPreferences sharedPreferences;
    private static final String mypreference = "myprefs";
    private Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(Global.LOG,"hier wird on Create"+controller);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (controller==null)
            controller = new Controller(this);
    }
    protected  void onDestroy(){
        super.onDestroy();
        Log.d(Global.LOG,"hier wird on Destroy");
    }

    protected void onResume(){
        super.onResume();
        Log.d(Global.LOG,"hier wird on Resume");
        controller.reset();
        sharedPreferences=getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        controller.loadPreferences(sharedPreferences);
    }

    protected void onPause(){
        controller.save(this);
        if (sharedPreferences!=null) {
            controller.savePreferences(sharedPreferences.edit());
            sharedPreferences.edit().apply();
        }

        Log.d(Global.LOG,"hier wird on Pause");
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        if (controller !=null)
            controller.addMenuIcon(this,menu.findItem(R.id.action_refresh));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id==R.id.menuItem_Settings) {
            @SuppressLint("InflateParams") final View view = LayoutInflater.from(this).inflate(R.layout.settings, null);
            final AlertDialog alrt = new AlertDialog.Builder(this).setView(view).create();
            alrt.setTitle("Settings");
            alrt.setIcon(R.mipmap.baseline_settings_black_24);
            controller.doSettings(view);
            alrt.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            alrt.show();
            return true;
        }
        else if (id==R.id.menu_Item_calibrate) {
            @SuppressLint("InflateParams") final View view = LayoutInflater.from(this).inflate(R.layout.calibration, null);
            final AlertDialog alrt = new AlertDialog.Builder(this).setView(view).create();
            alrt.setTitle("Calibration");
            alrt.setIcon(R.mipmap.baseline_build_black_24);
            DialogInterface.OnClickListener setEye = controller.reloadEye(view);
            alrt.setButton(AlertDialog.BUTTON_NEGATIVE, "CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            alrt.setButton(AlertDialog.BUTTON_POSITIVE, "OK",setEye);
            alrt.show();
            return true;
        }
        else if (id == R.id.menuItem_About) {
            @SuppressLint("InflateParams") View view = LayoutInflater.from(this).inflate(R.layout.about, null);
            final AlertDialog  alrt = new AlertDialog.Builder(this).setView(view).create();
            String versionName=BuildConfig.VERSION_NAME;
            int versionCode=BuildConfig.VERSION_CODE;
            ((TextView)view.findViewById(R.id.version))
                    .setText(String.format(Locale.getDefault(),"%s (%d)",versionName,versionCode));
            alrt.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    });
            alrt.setTitle("All In Gold");
            alrt.setIcon(R.mipmap.ic_launcher);
            alrt.show();


            return true;
        }
        if (id == R.id.menuItem_Clear) {
            final AlertDialog alrt = new AlertDialog.Builder(this).create();
            alrt.setTitle("Delete");
            alrt.setIcon(R.mipmap.baseline_delete_black_24);
            alrt.setMessage("Delete ALL or LAST");
            alrt.setButton(AlertDialog.BUTTON_NEUTRAL, "CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            alrt.setButton(AlertDialog.BUTTON_NEGATIVE, "LAST", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    controller.deleteLAST();

                }
            });
            alrt.setButton(AlertDialog.BUTTON_POSITIVE, "ALL",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int whichButton) {
                        controller.delete();
                        controller.reset();
                        }
                    });
            alrt.show();
            return true;
        }
        if (id == R.id.action_refresh){
            Toast.makeText(this,"Hallo",Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (controller!=null && !controller.undoo()) {
            if (snackbar==null||!snackbar.isShown()){
                snackbar = Snackbar.make(findViewById(R.id.fab), "Enter Arrows", Snackbar.LENGTH_SHORT);
                snackbar.setText("Press again to leave!");
                snackbar.setDuration(Snackbar.LENGTH_SHORT);
                snackbar.show();
            } else {
                snackbar.dismiss();
                super.onBackPressed();
            }
        }
    }

    void makeToast(String string){
        if (toast==null) {
            toast = Toast.makeText(this, string, Toast.LENGTH_SHORT);
        }
        toast.show();
    }


}