/*
all_in_gold, the master of archery sight calibration.
Copyright (C) 2019 Michael Schröder (mi.schroeder@netcologne.de)

This file is part of all_in_gold.

all_in_gold is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

all_in_gold is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with all_in_gold.  If not, see <http://www.gnu.org/licenses/>.
*/

package x653.all_in_gold;

//  Length_Unit verwaltet eine Längeneinheit

public enum Length_Unit {
    METER,YARD,STEP;

    public float getLength(){
        switch (this){
            case METER:
                return 100;
            case YARD:
                return (float) 109.3613;
            case STEP:
                return 80;
        }
        return 0;
    }

    public String toString(){
        switch (this){
            case YARD:
                return "yd";
            case METER:return "m";
            case STEP: return "step";
        }
        return "";
    }

}
